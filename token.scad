/*#################################################################################*\
 
	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
	                                           ░                              ░ 
	-----------------------------------------------------------------------------

	Developed by:			Jerry van Heerikhuize
	Modified by:            Jerry van Heerikhuize

	-----------------------------------------------------------------------------

	Version:                1.0.0
	Creation Date:          21/09/20
	Modification Date:      21/09/20
	Email:                  jvanheerikhuize@gmail.com
	Description:            Customizxable Token for open scad
    dependencies:           https://github.com/brodykenrick/text_on_OpenSCAD

\*#################################################################################*/

use <text_on/text_on.scad>

/*#################################################################################*\
    
    CONFIGURATION

\*#################################################################################*/

// token rotation
token_rotation = 0; //[0:360]

// The radius of the token excluding the rim
token_radius = 20;

// The height of the token
token_height = 5;

// the number of faces
token_faces = 60; //[3:360]



// the radius of the rim
rim_radius = 2;

// the height of the rim
rim_height = 1.5;



// the text on the top side
top_text = "TOP TEXT";

// the font of the text on the top side
top_text_font = "Impact";

// the size of the text on the top side
top_text_size = 5;

// the spacing of the text on the top side
top_text_spacing = 1;



// the text on the bottom side
bottom_text = "BOTTOM TEXT";

// the font of the text on the bottom side
bottom_text_font = "Impact";

// the size of the text on the bottom side
bottom_text_size = 5;

// the spacing of the text on the bottom side
bottom_text_spacing = 1;



// the text on the middle
middle_text = "1";

// the font of the text on the middle
middle_text_font = "Impact";

// the size of the text on the middle
middle_text_size = 15;

// the spacing of the text on the middle
middle_text_spacing = 1;

// the rotation of the text on the middle
middle_text_rotation = -90;



/*#################################################################################*\
    
    START

\*#################################################################################*/


// CREATE THE TOKEN

rotate (a = [0, 0, token_rotation]){
    
    //%cylinder($fn = token_faces, r = token_radius, h = token_height, center = false);
    cylinder($fn = token_faces, r = token_radius, h = token_height, center = false);

    // CREATE THE RIM
    difference() {

        cylinder($fn = token_faces, r = token_radius + rim_radius ,h=token_height +     rim_height, center = false);
    
        // the additional + 0.1 is a safety if you difference on the exact number a small layer is still present
        cylinder($fn = token_faces, r = token_radius, h=token_height + rim_height + 0.1, center=false);
    }

}

    // CREATE THE TEXT
    text_on_cylinder(top_text, [0,0,0], r = token_radius, h = token_height, size = top_text_size, face = "top", middle = 1, font = top_text_font, spacing = top_text_spacing);

    text_on_cylinder(bottom_text, [0,0,0], r = token_radius, h = token_height, size = bottom_text_size, face = "top", middle = 1, font = bottom_text_font, eastwest=180, spacing = bottom_text_spacing);

    text_on_cylinder(middle_text, [0,0,0], rotate = middle_text_rotation, h = token_height, size = middle_text_size, face = "top", middle = 1, font = middle_text_font, spacing = middle_text_spacing);


